
  let elementMenu = document.getElementById('elemHeader');

  let templateMenu = `<header>
  <div class="franja">
    <h2>Despachos en 48 horas en todas tus compras</h2>
  </div>
    <nav class="navbar navbar-dark bg-dark" id="navDelHeader">
        <button id="menu" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <img src="../../img/doux.png" alt="doux" class="ImgHeader">
        <div class="Home">

          <a href="../../Carrito/carrito.php"> <img src="../../img/Iconos/carrito.ico" class="IconoRedSoc"></a>    
          <a href="../../index.php"><img src="../../img/Iconos/home.ico" class="IconoRedSoc"></a> 
          <img src="../../img/Iconos/lupa.ico" class="IconoRedSoc" id="lupa">
        </div>

      </nav>
    <div class="pos-f-t">
      <div class="menu" id="navbarToggleExternalContent">
        <div class="bg-dark p-4">
            <a href="../../paginas/Cabello/1.html" class="EtiquetaMenu" id="EtuqACabello"><h4 class="text-white">Cabello</h4></a>
            <a href="../../paginas/Maquillaje/1.html" class="EtiquetaMenu" id="EtuqAMaquillaje"><h4 class="text-white">Maquillaje</h4></a>
            <a href="../../paginas/CuidadoCorporal/1.html" class="EtiquetaMenu" id="EtuqACuidadoCorporal"><h4 class="text-white">Cuidado Corporal</h4></a>
            <a href="../../paginas/Accesorios/1.html" class="EtiquetaMenu" id="EtuqAAccesorios"><h4 class="text-white">Accesorios</h4></a>
            <a href="../../paginas/unas/1.html" class="EtiquetaMenu" id="EtuqAUñas"><h4 class="text-white">Uñas</h4></a>
        </div>
      </div> 
    </div>
    <a href="../../sesiones/MiCuenta.php" class="cerrarSesion">[Ingresar a mi Cuenta]</a>


  <div class="header-top" id="inputBuscar">
      <div class="navegacion">
        <input type="search" placeholder="Buscar . . ." id="inputBusqueda" autocomplete="off">
      </div>
  </div>
  <div class="search" id="search">
      <table class="search-table" id="searchTable">
        <thead>
          <tr>
            <td></td>
          </tr>
        </thead>
        <tbody>
         <tr>
            <td><a href="../..//index.php" class="EtiqA">Inicio</a></td>
          </tr>


          <!-- -----------------------------------CABELLO---------------------- -->
          <tr>
            <td><a href="../..//paginas/Cabello/1.html" class="EtiqA">Cabello</a></td>
          </tr>
          <tr>
            <td><a href="../..//paginas/Cabello/CabelloProd1.html" class="EtiqA">Botox capilar Cola de Caballo</a></td>
          </tr>
          <tr>
            <td><a href="../..//paginas/Cabello/CabelloProd2.html" class="EtiqA">Botox capilar Biocauterizacion</a></td>
          </tr>
          <tr>
            <td><a href="../..//paginas/Cabello/CabelloProd3.html" class="EtiqA">Botox capilaro Efecto Liso</a></td>
          </tr>
          <tr>
            <td><a href="../..//paginas/Cabello/CabelloProd4.html" class="EtiqA">Botox capilaro Shock de Keratina</a></td>
          </tr>
          <tr>
            <td><a href="../..//paginas/Cabello/CabelloProd5.html" class="EtiqA">Botox capilaro Cabello Rizado</a></td>
          </tr>
          <tr>
            <td><a href="../..//paginas/Cabello/CabelloProd6.html" class="EtiqA">Botox capilaro Baño de Seda</a></td>
          </tr>
          <tr>
            <td><a href="../..//paginas/Cabello/CabelloProd7.html" class="EtiqA">Shampoo violeta</a></td>
          </tr>
          <tr>
            <td><a href="../..//paginas/Cabello/CabelloProd8.html" class="EtiqA">Shampoo azul</a></td>
          </tr>
          <tr>
            <td><a href="../..//paginas/Cabello/CabelloProd9.html" class="EtiqA">Aceite brillo capilar argan 60ml</a></td>
          </tr>
          <tr>
            <td><a href="../..//paginas/Cabello/CabelloProd10.html" class="EtiqA">Aceite brillo capilar coco 60ml</a></td>
          </tr>
          <tr>
            <td><a href="../..//paginas/Cabello/CabelloProd11.html" class="EtiqA">Aceite brillo capilar coco 30ml</a></td>
          </tr>
          <tr>
            <td><a href="../..//paginas/Cabello/CabelloProd12.html" class="EtiqA">Aceite brillo capilar argan 30ml</a></td>
          </tr>

          <!-- --------------------MAQUILLAJE-------------------------- -->
          <tr>
            <td><a href="../..//paginas/Maquillaje/1.html" class="EtiqA">Maquillaje</a></td>
          </tr>
          <tr>
            <td><a href="../..//paginas/Maquillaje/MaquillajeProd1.html" class="EtiqA">Brillo Labial</a></td>
          </tr>
          <tr>
            <td><a href="../..//paginas/Maquillaje/MaquillajeProd2.html" class="EtiqA">Delineador de ojos a prueba de agua</a></td>
          </tr>     
          <tr>
            <td><a href="../..//paginas/Maquillaje/MaquillajeProd3.html" class="EtiqA">CANDY labial liquido con color</a></td>
          </tr>    
          <tr>
            <td><a href="../..//paginas/Maquillaje/MaquillajeProd4.html" class="EtiqA">Labial en barra cremoso en tonos Matte</a></td>
          </tr>
          <tr>
            <td><a href="../..//paginas/Maquillaje/MaquillajeProd5.html" class="EtiqA">Mascara de pestañas 4D</a></td>
          </tr>
          <tr>
            <td><a href="../..//paginas/Maquillaje/MaquillajeProd6.html" class="EtiqA">Mascara de pestañas 4D</a></td>
          </tr>     
          <tr>
            <td><a href="../..//paginas/Maquillaje/MaquillajeProd7.html" class="EtiqA">Sombra de cejas</a></td>
          </tr>
          <tr>
            <td><a href="../..//paginas/Maquillaje/MaquillajeProd8.html" class="EtiqA">Base de maquillaje en crema</a></td>
          </tr>
          <tr>
            <td><a href="../..//paginas/Maquillaje/MaquillajeProd9.html" class="EtiqA">Rubor en polvo</a></td>
          </tr>
          <tr>
            <td><a href="../..//paginas/Maquillaje/MaquillajeProd10.html" class="EtiqA">Iluminador metalico en polvo</a></td>
          </tr>     
          <tr>
            <td><a href="../..//paginas/Maquillaje/MaquillajeProd11.html" class="EtiqA">Iluminador en polvo 3 en 1</a></td>
          </tr>
          <tr>
            <td><a href="../..//paginas/Maquillaje/MaquillajeProd12.html" class="EtiqA">Delineador de ojos Stars</a></td>
          </tr>
          <tr>
            <td><a href="../..//paginas/Maquillaje/MaquillajeProd13.html" class="EtiqA">Balsamo labial</a></td>
          </tr>    
          <tr>
            <td><a href="../..//paginas/Maquillaje/MaquillajeProd14.html" class="EtiqA">Labial en barra cremoso</a></td>
          </tr>
          <tr>
            <td><a href="../..//paginas/Maquillaje/MaquillajeProd15.html" class="EtiqA">Labial en barra cremoso</a></td>
          </tr>
          <tr>
            <td><a href="../..//paginas/Maquillaje/MaquillajeProd16.html" class="EtiqA">Sombras de ojos Montreal</a></td>
          </tr>
          <tr>
            <td><a href="../..//paginas/Maquillaje/MaquillajeProd17.html" class="EtiqA">Sombras de ojos Creations</a></td>
          </tr>
          <tr>
            <td><a href="../..//paginas/Maquillaje/MaquillajeProd18.html" class="EtiqA">Paleta contorno e iluminador</a></td>
          </tr>
          <tr>
            <td><a href="../..//paginas/Maquillaje/MaquillajeProd19.html" class="EtiqA">Sombras individuales gliter</a></td>
          </tr>                                                                                                                        

          <!-- ----------------------CUIDADO CORPORAL------------ -->
          <tr>
            <td><a href="../..//paginas/CuidadoCorporal/CuidadoCorpProd1.html" class="EtiqA">Exfoliante corporal</a></td>
          </tr>
          <tr>
            <td><a href="../..//paginas/CuidadoCorporal/CuidadoCorpProd2.html" class="EtiqA">Cera elastica miel</a></td>
          </tr>
          <tr>
            <td><a href="../..//paginas/CuidadoCorporal/CuidadoCorpProd3.html" class="EtiqA">Cera elastica aloe</a></td>
          </tr>
          <tr>
            <td><a href="../..//paginas/CuidadoCorporal/CuidadoCorpProd4.html" class="EtiqA">Exfoliante corporal formula vegana</a></td>
          </tr>
          <tr>
            <td><a href="../..//paginas/CuidadoCorporal/CuidadoCorpProd5.html" class="EtiqA">Crema facial con acido hialuronico</a></td>
          </tr>
          <tr>
            <td><a href="../..//paginas/CuidadoCorporal/CuidadoCorpProd6.html" class="EtiqA">Roll- on ojos</a></td>
          </tr> 
          <tr>
            <td><a href="../..//paginas/CuidadoCorporal/CuidadoCorpProd7.html" class="EtiqA">Mascarilla facial de limpieza profunda</a></td>
          </tr>
          <tr>
            <td><a href="../..//paginas/CuidadoCorporal/CuidadoCorpProd8.html" class="EtiqA">Suero antiarrugas rosas</a></td>
          </tr>
          <tr>
            <td><a href="../..//paginas/CuidadoCorporal/CuidadoCorpProd9.html" class="EtiqA">Crema tratamiento de ojos con aloe vera</a></td>
          </tr> 
          <tr>
            <td><a href="../..//paginas/CuidadoCorporal/CuidadoCorpProd10.html" class="EtiqA">Suero antiarrugas</a></td>
          </tr>
          <tr>
            <td><a href="../..//paginas/CuidadoCorporal/CuidadoCorpProd11.html" class="EtiqA">Mascarilla negra</a></td>
          </tr>
          <tr>
            <td><a href="../..//paginas/CuidadoCorporal/CuidadoCorpProd12.html" class="EtiqA">Mascarilla hidratante para labios</a></td>
          </tr>  
          <tr>
            <td><a href="../..//paginas/CuidadoCorporal/CuidadoCorpProd13.html" class="EtiqA">Cremas para manos</a></td>
          </tr>
          <tr>
            <td><a href="../..//paginas/CuidadoCorporal/CuidadoCorpProd14.html" class="EtiqA">Desmaquillante de baba de caracol</a></td>
          </tr>  
          <tr>
            <td><a href="../..//paginas/CuidadoCorporal/CuidadoCorpProd15.html" class="EtiqA">Mascara facial hidratante</a></td>
          </tr>  
          <tr>
            <td><a href="../..//paginas/CuidadoCorporal/CuidadoCorpProd16.html" class="EtiqA">Desmaquillante de Aloe vera</a></td>
          </tr>  
          <tr>
            <td><a href="../..//paginas/CuidadoCorporal/CuidadoCorpProd17.html" class="EtiqA">Crema Hidratante</a></td>
          </tr>                                                                                                                                                          

          <!-- ----------------------------------UÑAS-------------------- -->
          <tr>
            <td><a href="../..//paginas/unas/UnasProd1.html" class="EtiqA">Polvo de polimero</a></td>
          </tr> 
          <tr>
            <td><a href="../..//paginas/unas/UnasProd2.html" class="EtiqA">Liquido de acrilico uso profesional</a></td>
          </tr> 
          <tr>
            <td><a href="../..//paginas/unas/UnasProd3.html" class="EtiqA">Liquido removedor de acrilico</a></td>
          </tr> 
          <tr>
            <td><a href="../..//paginas/unas/UnasProd4.html" class="EtiqA">Taco pulidor de uñas</a></td>
          </tr> 
          <tr>
            <td><a href="../..//paginas/unas/UnasProd5.html" class="EtiqA">Hidratante de cuticulas</a></td>
          </tr>    
          <tr>
            <td><a href="../..//paginas/unas/UnasProd6.html" class="EtiqA">Petalos quita esmalte</a></td>
          </tr> 
          <tr>
            <td><a href="../..//paginas/unas/UnasProd7.html" class="EtiqA">Esmaltes tradicionales ColorKeep</a></td>
          </tr> 
          <tr>
            <td><a href="../..//paginas/unas/UnasProd8.html" class="EtiqA">Esmaltes tradicionales Gliter</a></td>
          </tr>                                                                                           

          <!-- ----------------------------------ACCESORIOS------------- -->
          <tr>
            <td><a href="../..//paginas/Accesorios/AccesoriosProd1.html" class="EtiqA">Set pinceles simple</a></td>
          </tr>
          <tr>
            <td><a href="../..//paginas/Accesorios/AccesoriosProd2.html" class="EtiqA">Encrespador</a></td>
          </tr>
          <tr>
            <td><a href="../..//paginas/Accesorios/AccesoriosProd3.html" class="EtiqA">Espejos de cartera</a></td>
          </tr>
          <tr>
            <td><a href="../..//paginas/Accesorios/AccesoriosProd4.html" class="EtiqA">Esponjas de maquillaje</a></td>
          </tr>                                                                                                                                                                                                                                                                                                                                                                                                                                

        </tbody>
      </table>
  </div> 
</header>`;


elementMenu.innerHTML =templateMenu;

//reenplace ../../ por ../../  por si hay que volverlos a lo de antes