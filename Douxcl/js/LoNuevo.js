let elementLoNuevo = document.getElementById('elemLoNuevo');

  let templateLoNuevo = `      <div class="Minicarrusel">
  <h3 id="LoNuevo">Lo Nuevo</h3>
  <div id="carouselExampleIndicators2" class="carousel slide" data-bs-ride="carousel">

       <div class="carousel-indicators BotonesDesplazaCarrusel">
           <button type="button" data-bs-target="#carouselExampleIndicators2"data-bs-slide-to="0" class="active" aria-current="true"aria-label="Slide 1"></button>
           <button type="button" data-bs-target="#carouselExampleIndicators2" data-bs-slide-to="1" aria-label="Slide 2"></button>
           <button type="button" data-bs-target="#carouselExampleIndicators2" data-bs-slide-to="2" aria-label="Slide 3"></button>
       </div>
         <div class="carousel-inner">

             <div class="carousel-item active DivImgCarrusel" alt="...">

                 <div class="imgMiniCarrusel">
                     <a href="../..//paginas/Accesorios/AccesoriosProd1.html"><img src="../../img/accesorios/1.jpg" alt="Coast" class="ajustarImg"></a>
                     <h4 class="NomProd">Set pinceles simple</h4>
                     <!-- <h5>Por unidad $1.890</h5> -->
                     <h5>Por mayor $900</h5> 
                 </div> 

                 <div class="imgMiniCarrusel">
                     <a href="../..//paginas/CuidadoCorporal/CuidadoCorpProd10.html"><img src="../../img/cuidadoCorporal/19.jpg" alt="Coast" class="ajustarImg"></a>
                     <h4 class="NomProd">Suero antiarrugas.</h4>
                     <!-- <h5>Por unidad $3.990</h5> -->
                     <h5>Por mayor $1.800</h5> 
                 </div> 

                 <div class="imgMiniCarrusel">
                     <a href="../..//paginas/CuidadoCorporal/CuidadoCorpProd15.html"><img src="../../img/cuidadoCorporal/28.jpg" alt="Coast" class="ajustarImg"></a>
                     <h4 class="NomProd">Máscara facial hidratante</h4>
                     <!-- <h5>Por unidad $1.490</h5> -->
                     <h5>Por mayor $700</h5> 
                 </div>                
             </div>
             <div class="carousel-item DivImgCarrusel" alt="...">

                 <div class="imgMiniCarrusel">
                     <a href="../..//paginas/Maquillaje/MaquillajeProd18.html"><img src="../../img/maquillaje/42.jpg" alt="Coast" class="ajustarImg"></a>
                     <h4 class="NomProd">Paleta contorno e iluminador</h4>
                     <!-- <h5>Por unidad $2.490</h5> -->
                     <h5>Por mayor $1.200</h5> 
                 </div> 
                 <div class="imgMiniCarrusel">
                     <a href="../..//paginas/unas/UnasProd7.html"><img src="../../img/unas/15.jpg" alt="Coast" class="ajustarImg"></a>
                     <h4 class="NomProd">Esmaltes tradicionales ColorKeep</h4>
                     <!-- <h5>Por unidad $990</h5> -->
                     <h5>Por 12 unidades $4.800</h5> 
                 </div> 
                 <div class="imgMiniCarrusel">
                     <a href="../..//paginas/CuidadoCorporal/CuidadoCorpProd17.html"><img src="../../img/cuidadoCorporal/31.jpg" alt="Coast" class="ajustarImg"></a>
                     <h4 class="NomProd">Crema Hidratante</h4>
                     <!-- <h5>Por unidad $2.990</h5> -->
                     <h5>Por mayor $1.500</h5> 
                 </div>  
             </div>
       </div>
       <button class="carousel-control-prev PreviousNext" type="button" data-bs-target="#carouselExampleIndicators2"  data-bs-slide="prev" >
         <span class="carousel-control-prev-icon" aria-hidden="true"></span>
         <span class="visually-hidden">Previous</span>
       </button>
       <button class="carousel-control-next PreviousNext" type="button" data-bs-target="#carouselExampleIndicators2"  data-bs-slide="next">
         <span class="carousel-control-next-icon" aria-hidden="true" id="Next"></span>
         <span class="visually-hidden">Next</span>
       </button>
   </div>
</div>
`;


elementLoNuevo.innerHTML =templateLoNuevo;