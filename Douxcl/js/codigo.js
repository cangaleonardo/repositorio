$('#menu').click(function(){
	
	if(document.getElementById("navbarToggleExternalContent").style.display == "block"){
		$("#navbarToggleExternalContent").css("display", "none");
	}else
	{
		$("#navbarToggleExternalContent").css("display", "block");
		$("#inputBuscar").css("display", "none");
		$("#search").css("display", "none");
	}

});
$('#lupa').click(function(){
	
	if(document.getElementById("inputBuscar").style.display == "block"){
		$("#inputBuscar").css("display", "none");
		$("#search").css("display", "none");
	}else
	{
		$("#inputBuscar").css("display", "block");
		$("#navbarToggleExternalContent").css("display", "none")
		$("#inputBusqueda").focus();
	}

});

$(document).click(function(){//que la hacer click en cualquier lado todo lo de input vueva a "" y sacara la pantalla gris

	$('#inputBusqueda').val("");
	$("header").css({
		"height": "auto",
		"background": "none"
	})	

});

// --------------------------buscar-------------------

var consulta = $("#searchTable").DataTable();

$("#inputBusqueda").keyup(function(){
	consulta.search($(this).val()).draw();

	$("header").css({
		"height": "100vh",
		"background": "rgba(0,0,0,0.5)"
	})

	if ($("#inputBusqueda").val() == ""){
		$("header").css({
			"height": "auto",
			"background": "none"
		})

		$("#search").hide();

	} else {
		$("#search").fadeIn("fast");
	}

})

$("#inputBusqueda").keypress(function(e){//cuando se preciona una tecla salta la funcion
			//contamos las filas que se mostraran
			   var nFilas = parseInt($("#search tr").length,10)-1;//convierto a numero(resto 1 por que cuenta 1 de mas)
		       if((e.which == 13)&&(nFilas=="1"))
		       {   //e.which=13 es la tecla enter, sino toma cualquiera
					// $("#inputBuscar").css("display", "none");
					// $("#search").css("display", "none");
					// var href = $('.EtiqA').attr('href')//guarda en la variable href el gref de .EtiqA
					// alert(href);
					$(location).attr('href',$('.EtiqA').attr('href'));//nos redirecciona a al lugar indicado entre ()
					// HASTA AQUI LLEGAMOS, AHORA HAY QUE RECUPERAR EL VALOR DE <a> E ir a esa url
		   		}

})
// --------------------------------efecto equiquetas Menu-----------------------------

$("#EtuqACabello").ready(function(){
	$("#EtuqACabello").hover(function(){
				$("#EtuqACabello").css("text-decoration", "underline");
		}, function(){
			$("#EtuqACabello").css("text-decoration", "none");
		});
});
$("#EtuqAMaquillaje").ready(function(){
	$("#EtuqAMaquillaje").hover(function(){
				$("#EtuqAMaquillaje").css("text-decoration", "underline");
		}, function(){
			$("#EtuqAMaquillaje").css("text-decoration", "none");
		});
});
$("#EtuqACuidadoCorporal").ready(function(){
	$("#EtuqACuidadoCorporal").hover(function(){
				$("#EtuqACuidadoCorporal").css("text-decoration", "underline");
		}, function(){
			$("#EtuqACuidadoCorporal").css("text-decoration", "none");
		});
});
$("#EtuqAAccesorios").ready(function(){
	$("#EtuqAAccesorios").hover(function(){
				$("#EtuqAAccesorios").css("text-decoration", "underline");
		}, function(){
			$("#EtuqAAccesorios").css("text-decoration", "none");
		});
});
$("#EtuqAUñas").ready(function(){
	$("#EtuqAUñas").hover(function(){
				$("#EtuqAUñas").css("text-decoration", "underline");
		}, function(){
			$("#EtuqAUñas").css("text-decoration", "none");
		});
});

// ---------------------login-----------------
// $('#Entrar').click(function(){
// 	alert("al entrar llega");
// });


// ----------registrarse-------------
// $('#Registrarse').click(function(){
// 	location.href = "conexion.php";;
// });

// --------------------------------------------micuenta---------------------------------------
$('#editNom').click(function(){
	$('#FormNombre').css("display", "block");
	$("#nombre").focus();
	$("#FormApellido").css("display", "none");
	$("#FormCorreo").css("display", "none");
});
$('#editApellido').click(function(){
	$("#FormApellido").css("display", "block");
	$("#apellido").focus();
	$("#FormCorreo").css("display", "none");
	$('#FormNombre').css("display", "none");
});
$('#editCorreo').click(function(){
	$("#FormCorreo").css("display", "block");
	$("#correo").focus();
	$('#FormNombre').css("display", "none");
	$("#FormApellido").css("display", "none");

});