
  let elementFooter = document.getElementById('ElemFooter');

  let templateFooter = `<footer class="section footer-classic context-dark bg-image">
                      <div class="container">
                          <div class="col-md-4 col-xl-3" id="siganos">
                              <br>
                                <div class="col"><a href="https://www.facebook.com/douxtb/" target="_blank"><img src="../..//img/Iconos/facebook.png" alt="facebook" class="IconoRedSoc"></a> </div>
                                <div class="col"><a href="https://z-p3.www.instagram.com/douxtb/" target="_blank"><img src="../..//img/Iconos/instagram.png" alt="instagram"class="IconoRedSoc"></a></div>
                                <div class="col"><a href="https://www.tiktok.com/@douxtb?lang=es" target="_blank"><img src="../..//img/Iconos/tik_tok.ico" alt="twitter"class="IconoRedSoc"></a>    </div>
                          </div>
                          <div class="row row-30">
                              <div class="col-md-4 col-xl-5 DerechosR">
                                <div class="pr-xl-4">
                                  <br>
                                  <!-- Rights-->
                                  <p class="rights"><span>©  </span><span class="copyright-year">2021</span><span> </span><span>Doux</span><span>. </span><span>Derechos reservados</span></p>
                                </div>
                              </div>
                              <br>
                              <div class="col-md-4 ComoComprar">
                                <br>
                                <!-- <h5>Servicio al Cliente</h5> -->
                                <a href="../..//paginas/QuienesSomos.html"><h5>¿Quiénes Somos?</h5></a>
                                <a href="../..//paginas/ComoComprar.html"><h5>¿Cómo comprar?</h5></a>
                                <a href="../..//paginas/PreguntasFrecuentes.html"><h5>Preguntas frecuentes</h5></a>
                                </div>
                              </div>
                          </div>
                  </footer`;


elementFooter.innerHTML =templateFooter;

