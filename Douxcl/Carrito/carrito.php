<?php
include_once '../conexion.php';
$TotalXpagar=0;

session_start();
if(isset ($_SESSION['usuario'])){

    $correo= $_SESSION['usuario'];

    $sql_leer = 'SELECT * FROM compras WHERE usuario = ?';

    $gsent = $pdo->prepare($sql_leer);
    $gsent->execute(array($correo));


    $resultado = $gsent->fetchAll();

// var_dump($resultado);

?>
    <!DOCTYPE html>
    <html>

    <head>
      <meta charset="utf-8">
      <meta name="author" content="Leonardo Exequiel Canga Salinas" />
      <meta name="copyright" content="Leonardo Exequiel Canga Salinas" />

            
      <title>Doux</title>

      <link rel="stylesheet" href="../bootstrap-5.0.0-beta2-dist/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="..\css\estilos.css">
        <link rel="stylesheet" href="..\css\whatsapp.css">
        <link  rel="icon"   href="..\img\Iconos\doux.ico" type="" />
      <link rel="stylesheet" type="text/css" href="../css/EstilosMax750.css">   

      <!-- <link  rel="icon"   href="" type="" /> -->
      <link rel="shortcut icon" type="image/x-icon" href="../img\Iconos\doux.ico"/>

    </head>
    <body>
    <!-- --------------------------------boton whatsapp--------------------------- -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
            <a href="https://api.whatsapp.com/send?phone=56936258826&text=Hola%21%20Quisiera%20m%C3%A1s%20informaci%C3%B3n%20sobre%20el%20producto..." class="float" target="_blank">
            <i class="fa fa-whatsapp my-float"></i>
            </a> -->

    <!-- --------------------------------header----------------------------------- -->
            <div id="elemHeader"></div>
            <script src="../js\menu.js"></script>

    <!-- --------------------------------------carrusel---------------------------- -->
            <!-- <div id="elemCarrusel"></div>
            <script src="../js/carrusel.js">                 
            </script> -->
    <!-- --------------------------------------Login--------------------------->
    <h1 id="textCarrito">Carrito de compras</h1>
      <?php foreach($resultado as $dato): ?>    
            <div class="container mt-5 contenedorCarrito">   
                  <div class="row alert alert-primary align-items-center">

                              <div class="col-md-2">
                              <img  class="ImgCarrito" src="../<?php echo $dato['img']?>" alt="">

                              </div>
                              <div class=" col-md-9 " role="alert">

                                    <p> Nombre del Producto: <?php echo $dato['nombre'] ?></p>
                                    <p> Precio Unitario: $<?php echo $dato['precio']?></p>
                                    <p> Cantidad de Unidades: <?php echo $dato['cantidad']?></p>
                                    <p> Agregado el: <?php echo $dato['fechaYhora']?></p>
                                    <p> Total: $<?php echo $dato['total'];?></p>
                                    
                                    <?php $TotalXpagar=$TotalXpagar+$dato['total'] ?>
                              </div>
                              <div class=" col-md-1">
                              <a href="BorrarCompra.php?id=<?php echo $dato['id'] ?>"> <img src="../img/Iconos/borrar.ico" class="IconoRedSoc"></a>    

                              </div>
                </div>             
            </div>
            
      <?php endforeach?>
      <div class="container mt-5 contenedorCarrito DivBoton">
                  <div class="row  justify-content-start ">
                    <div class="col-md-10 align-self-center">
                      <h1>Total a Pagar: $ <?php echo $TotalXpagar?></h1>
                    </div>
                  </div>

                  <div class="row  justify-content-end ">
                    <div class="col-md-3 align-self-center">
                      <button class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2 col-md-12" type="submit" id="Comprar">Comprar</button>
                    </div>
                  </div>
      </div>     
    <!-- --------------------------------------carrusel-2--------------------------->
    <div id="elemLoNuevo"></div>
          <script src="../js/LoNuevo.js"></script>

    <!-- ------------------------------------------------footer-------------------------------- -->

            <div id="ElemFooter"></div>
            <script src="../js/footer.js"></script>
    </body>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
      <script src="../js/jquery.dataTables.min.js"></script>
      <script type="text/javascript" src="../js/codigo.js"></script>
    <!-- 
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script> -->

      <script type="text/javascript" src="../bootstrap-5.0.0-beta2-dist/js/bootstrap.bundle.min.js"></script>
      
    </html>
<?php
 
    }else{
      // $urlAnterior= $_SERVER["HTTP_REFERER"];

      echo "<script>alert ('Debes iniciar sesion para acceder a tu carrito');
      location.href = '../../sesiones/login.php';
      </script>";
    }
 
?>