<?php

 
?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="author" content="Leonardo Exequiel Canga Salinas" />
	<meta name="copyright" content="Leonardo Exequiel Canga Salinas" />
        
	<title>Doux</title>
  <link rel="stylesheet" href="../bootstrap-5.0.0-beta2-dist/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="..\css\estilos.css">
  <link rel="stylesheet" href="..\css\whatsapp.css">
  <link  rel="icon"   href="..\img\Iconos\doux.ico" type="" />
  <link rel="stylesheet" type="text/css" href="../css/EstilosMax750.css">   

  <!-- <link  rel="icon"   href="" type="" /> -->
  <link rel="shortcut icon" type="image/x-icon" href="../img\Iconos\doux.ico"/>

</head>
<body>
<!-- --------------------------------boton whatsapp--------------------------- -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <a href="https://api.whatsapp.com/send?phone=56936258826&text=Hola%21%20Quisiera%20m%C3%A1s%20informaci%C3%B3n%20sobre%20el%20producto..." class="float" target="_blank">
        <i class="fa fa-whatsapp my-float"></i>
        </a>

<!-- --------------------------------header----------------------------------- -->
        <div id="elemHeader"></div>
        <script src="../js\menu.js"></script>


<!-- --------------------------------------Login--------------------------->
<div class="container-fluid login-contenedor">
  <div class="row no-gutter justify-content-center login-subcontenedor">
    <!-- <div class="d-none d-md-flex col-md-4 col-lg-6 bg-image"></div> -->
    <div class="col-md-8 col-lg-6">
      <div class="login d-flex align-items-center py-5">
        <div class="container">
          <div class="row">
            <div class="col-md-9 col-lg-8 mx-auto">
              <h3 class="login-heading mb-4">Complete los siguientes campos para cambiar su contraseña</h3>
              <form method="POST" action="ActualizarPas.php">

                <!-- <div class="form-label-group">
                  <input name="correo" type="email" id="inputEmail" class="form-control" placeholder="Ej:adrianG19@gmail.com" required autofocus>
                  <label for="inputEmail">Dirección de correo electrónico</label>
                </div> -->

                <div class="form-label-group">
                  <input name="contraseña" type="password" id="inputPassword" class="form-control" placeholder="Password" required>
                  <label for="inputPassword">Nueva Contraseña</label>
                </div>

                <div class="form-label-group">
                  <input name="contraseña2" type="password" id="inputPassword2" class="form-control" placeholder="Password" required>
                  <label for="inputPassword2">Repita la Contraseña</label>
                </div>

                <button class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2" type="submit" id="Aceptar">Aceptar</button>

              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- --------------------------------------carrusel-2--------------------------->
<div id="elemLoNuevo"></div>
      <script src="../js/LoNuevo.js"></script>

<!-- ------------------------------------------------footer-------------------------------- -->

        <div id="ElemFooter"></div>
        <script src="../js/footer.js"></script>
</body>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="../js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="../js/codigo.js"></script>
<!-- 
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script> -->

  <script type="text/javascript" src="../bootstrap-5.0.0-beta2-dist/js/bootstrap.bundle.min.js"></script>
  
</html>