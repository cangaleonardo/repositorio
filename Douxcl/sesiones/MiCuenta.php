<?php
session_start();
if(isset ($_SESSION['usuario'])):

    $correo= $_SESSION['usuario'];

    include_once '../conexion.php';


    $sql = 'SELECT * FROM usuarios WHERE correo = ?';
    $sentencia = $pdo->prepare($sql);
    $sentencia->execute(array($correo));
    $resultado = $sentencia->fetch();
    // echo '<script>alert (" correo '.$resultado['correo'].' contraseña '.$resultado['pass'].'");</script>';
?>
    <!DOCTYPE html>
    <html>

    <head>
        <meta charset="utf-8">
        <meta name="author" content="Leonardo Exequiel Canga Salinas" />
        <meta name="copyright" content="Leonardo Exequiel Canga Salinas" />
            
        <title>Doux</title>
    <link rel="stylesheet" href="../bootstrap-5.0.0-beta2-dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="..\css\estilos.css">
    <link rel="stylesheet" href="..\css\whatsapp.css">
    <link  rel="icon"   href="..\img\Iconos\doux.ico" type="" />
    <link rel="stylesheet" type="text/css" href="../css/EstilosMax750.css">   

    <!-- <link  rel="icon"   href="" type="" /> -->
    <link rel="shortcut icon" type="image/x-icon" href="../img\Iconos\doux.ico"/>

    </head>
    <body>
    <!-- --------------------------------boton whatsapp--------------------------- -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
            <a href="https://api.whatsapp.com/send?phone=56936258826&text=Hola%21%20Quisiera%20m%C3%A1s%20informaci%C3%B3n%20sobre%20el%20producto..." class="float" target="_blank">
            <i class="fa fa-whatsapp my-float"></i>
            </a>
    <!-- --------------------------------datos--------------------------- -->
    <div class="container Cont-Mens-login text-center" id="galeria">

        <div class="row alert alert-light align-items-center">
            <div class=" col-md-11 " role="alert">
                  <h1 class="text-dark"><strong> Tu cuenta</strong></h1>           
            </div>
        </div>
        <div class="row alert alert-light align-items-center">
                        <div class=" col-md-11" role="alert">
                             <h3>Nombre: <?php echo $resultado['nombre']?></h3>
                        </div>
                        <div class=" col-md-1">
                            <a id="editNom" href="#"> <img src="../img/Iconos/editar.ico" class="IconoRedSoc"></a>    
                        </div>                
        </div>
        <form method="POST" action="EditarNombre.php" id="FormNombre">
            <div class="form-label-group">
                <input name="nombre" type="text" id="nombre" class="form-control"  required autofocus>
                <label for="inputEmail">Ingrese su nuevo Nombre</label>
            </div>
            <button class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2" type="submit" id="cambiar">Cambiar</button>
        </form>



        <div class="row alert alert-light align-items-center">
                        <div class=" col-md-11 " role="alert">
                                <h3>Apellido: <?php echo $resultado['apellido']?></h3>
                        </div>
                        <div class=" col-md-1">
                            <a id="editApellido" href="#"> <img src="../img/Iconos/editar.ico" class="IconoRedSoc"></a>    
                        </div>
        </div>
        <form method="POST" action="EditarApellido.php" id="FormApellido">
 
            <div class="form-label-group">
                <input name="apellido" type="text" id="apellido" class="form-control" required autofocus>
                <label for="inputEmail">Ingrese su nuevo Apellido</label>
            </div>
            <button class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2" type="submit" id="cambiar">Cambiar</button>
        </form>




        <div class="row alert alert-light align-items-center">
                        <div class=" col-md-11 " role="alert">
                              <h3>Correo: <?php echo $resultado['correo']?></h3>
                        </div>
                        <!-- <div class=" col-md-1">
                            <a id="editCorreo" href="#"> <img src="../img/Iconos/editar.ico" class="IconoRedSoc"></a>    
                        </div> -->
        </div>
        

        <div class="row alert alert-light align-items-center">
                        <div class=" col-md-11 " role="alert">
                              <h3>Contraseña: ********</h3>
                        </div>
                        <div class=" col-md-1">
                            <a href="CambiarPass.php"> <img src="../img/Iconos/editar.ico" class="IconoRedSoc"></a>    
                        </div>
        </div>

        <div class="row alert alert-light align-items-center">
            <div class=" col-md-11 " role="alert">
                  <a href="CerrarSesion.php"><h3><ins><strong>Cerrar Sesion</strong></ins></h3></a>            
            </div>
        </div>
        <!-- <div class="alert alert-primary" role="alert">
        </div> -->





    </div>

    <!-- --------------------------------header----------------------------------- -->
            <div id="elemHeader"></div>
            <script src="../js\menu.js"></script>

    <!-- ------------------------------------------------footer-------------------------------- -->

            <div id="ElemFooter"></div>
            <script src="../js/footer.js"></script>
    </body>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="../js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../js/codigo.js"></script>
    <!-- 
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script> -->

    <script type="text/javascript" src="../bootstrap-5.0.0-beta2-dist/js/bootstrap.bundle.min.js"></script>
    
    </html>
<?php
endif;
if(!(isset ($_SESSION['usuario']))){
    
    $urlAnterior= $_SERVER["HTTP_REFERER"];

    echo "<script>alert ('Aun no has iniciado sesion');
    location.href = 'login.php';
    </script>";
}

 
?>