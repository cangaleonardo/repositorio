<?php
    include_once '../conexion.php';

    $urlAnterior= $_POST['urlActual'];

    $nombre = $_POST['Nombre'];
    $apellido = $_POST['Apellido'];
    $correo = $_POST['Email'];
    $contraseña = $_POST['password'];
    $contraseña2 = $_POST['password2'];

    $contraseña = password_hash($contraseña, PASSWORD_DEFAULT);
    // echo '<pre>';
    // var_dump($nombre);
    // var_dump($apellido);
    // var_dump($correo);
    // var_dump($contraseña);
    // var_dump($contraseña2);
    // echo '</pre>';
    // die();

    $sql = 'SELECT * FROM usuarios WHERE correo = ?';
    $sentencia = $pdo->prepare($sql);
    $sentencia->execute(array($correo));
    $resultado = $sentencia->fetch();
    // var_dump($resultado);
    // die();



    if($resultado){
        header('location:CuentaExistente.php');
        die();
    }

    if (password_verify($contraseña2, $contraseña)) {
        // echo '¡La contraseña es válida!';

        $sql_agregar = 'INSERT INTO usuarios (nombre,apellido,correo,pass) VALUES (?,?,?,?)';
        $sentencia_agregar = $pdo->prepare($sql_agregar);
        if($sentencia_agregar->execute(array($nombre,$apellido,$correo,$contraseña))){
            header("location:RegistroExitoso.php?urlActual=$urlAnterior");
        }

        $sentencia_agregar = null;//esta y la linea siguiente cierran sesion de bd
        $pdo = null;

        // echo 'Agregado';
        // header('location:index.php');
    }
    else {
        // echo '<script type="text/javascript">alert("las contraseñas no conciden");
        // location.href = "registrarse.php";
        // </script>';
        header("location:registrarse2.php?nombre=$nombre&apellido=$apellido&correo=$correo");
    }
?>
