<?php
session_start();

include_once '../conexion.php';

 $correo =$_SESSION['usuario'];
 $password = $_POST['contraseña'];
 $password2 = $_POST['contraseña2'];

 $password = password_hash($password, PASSWORD_DEFAULT);

 $sql = 'SELECT * FROM usuarios WHERE correo = ?';
 $sentencia = $pdo->prepare($sql);
 $sentencia->execute(array($correo));
 $resultado = $sentencia->fetch();
 // var_dump($resultado);

 if($resultado){
    if (password_verify($password2, $password)) {
        // echo '¡La password es válida!';
        $sql_actualizar = 'UPDATE usuarios SET pass=? WHERE correo = ?';
        $sentencia_actualizar= $pdo->prepare($sql_actualizar);
        $sentencia_actualizar->execute(array($password, $correo));
    
        $sentencia_actualizar = null;//esta y la linea siguiente cierran sesion de bd
        $pdo = null;
    
        header('location:CambioPassExitoso.php');

    }else {
        echo '<script>alert ("salio por el else");
        </script>';
        header("location:CambiarPass2.php?correo=$correo");
    }
    
 }else{
    echo '<script>alert ("El correo '.$correo.' No es valido");
    location.href = "CambiarPass.php";
    </script>';

 }



 
?>