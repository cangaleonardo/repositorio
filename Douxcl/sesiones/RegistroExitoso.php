<?php
 
 $urlAnterior= $_GET['urlActual'];
?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="author" content="Leonardo Exequiel Canga Salinas" />
	<meta name="copyright" content="Leonardo Exequiel Canga Salinas" />
        
	<title>Doux</title>
  <link rel="stylesheet" href="../bootstrap-5.0.0-beta2-dist/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="..\css\estilos.css">
  <link rel="stylesheet" href="..\css\whatsapp.css">
  <link rel="stylesheet" type="text/css" href="../css/EstilosMax750.css">   

  <link  rel="icon"   href="..\img\Iconos\doux.ico" type="" />
  <!-- <link  rel="icon"   href="" type="" /> -->
  <link rel="shortcut icon" type="image/x-icon" href="../img\Iconos\doux.ico"/>

</head>
<body>
<!-- --------------------------------boton whatsapp--------------------------- -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <a href="https://api.whatsapp.com/send?phone=56936258826&text=Hola%21%20Quisiera%20m%C3%A1s%20informaci%C3%B3n%20sobre%20el%20producto..." class="float" target="_blank">
        <i class="fa fa-whatsapp my-float"></i>
        </a>

<!-- --------------------------------header----------------------------------- -->
        <div id="elemHeader"></div>
        <script src="../js\menu.js"></script>


<!-- ------------------------------------galeria--------------------------------- -->
<div class="container Cont-Mens-login " id="galeria">

<h1 class="" >¡Felicidades!</h1>

<div class="tz-gallery galleryCC">

          <h2>
            Acabas de registrarte exitosamente a Doux.
          </h2>
          <h2>Ahora ya puedes <a href="login.php?urlActual=<?php echo $urlAnterior ?>">Iniciar Sesion</a> con tu nueva cuenta</h2>
          <h2>¡MUCHAS GRACIAS POR ELEGIR DOUX!</h2>

</div>

</div>

<!-- --------------------------------------carrusel-2--------------------------->
<div id="elemLoNuevo"></div>
      <script src="../js/LoNuevo.js"></script>

<!-- ------------------------------------------------footer-------------------------------- -->

        <div id="ElemFooter"></div>
        <script src="../js/footer.js"></script>
</body>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="../js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="../js/codigo.js"></script>
<!-- 
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script> -->

  <script type="text/javascript" src="../bootstrap-5.0.0-beta2-dist/js/bootstrap.bundle.min.js"></script>
  
</html>