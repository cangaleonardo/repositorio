<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

include_once '../conexion.php';


 $correo=$_POST['correo'];

 //VERIFICAMOS SI EL USUARIO EXISTE
 $sql = 'SELECT * FROM usuarios WHERE correo = ?';
 $sentencia = $pdo->prepare($sql);
 $sentencia->execute(array($correo));
 $resultado = $sentencia->fetch();
 // echo '<script>alert (" correo '.$resultado['correo'].' contraseña '.$resultado['pass'].'");</script>';
 if(!$resultado)
 {
  echo '<script>alert ("El Correo Ingresado no se encuentra Registrado");
  location.href = "RecuperarPass.php"
  </script>';

 }else{

    $cadena = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";//{
    $longitudCadena=strlen($cadena);                 //
    $pass = "";                                      //
    $longitudPass=6;                                 // generamos claves aleatorias;
    for($i=1 ; $i<=$longitudPass ; $i++){            //
        $pos=rand(0,$longitudCadena-1);              //
        $pass .= substr($cadena,$pos,1);             //}
    }
    //$pass sera la nueva contraseña generada aleatoriamente

    $NuevaContraseña = password_hash($pass, PASSWORD_DEFAULT);//codificamos la nueva contraseña para guardarla en la bd

    $sql_actualizar = 'UPDATE usuarios SET pass=? WHERE correo = ?';
    $sentencia_actualizar= $pdo->prepare($sql_actualizar);
    $sentencia_actualizar->execute(array($NuevaContraseña, $correo));

    $sentencia_actualizar = null;//esta y la linea siguiente cierran sesion de bd
    $pdo = null;

    // echo  "la contraseña de correo";
    // echo $correo;
    // echo "fue actualizada";

        

    require 'PHPMailer-master/Exception.php';
    require 'PHPMailer-master/PHPMailer.php';
    require 'PHPMailer-master/SMTP.php';

    //Instantiation and passing `true` enables exceptions
    $mail = new PHPMailer(true);

    try {
        //Server settings
        $mail->SMTPDebug = 0;                      //(si pones 2 te mostrara los detalles del envio)
        $mail->isSMTP();                                            //Send using SMTP
        $mail->Host       = 'smtp.gmail.com';                       //aca va el tipo de correo que se udara(Este se usa para gmail)
        $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
        $mail->Username   = 'douxtb@gmail.com';                     //(aca hayq que poner el correo con el que iniciaremos sesion para enviar correo)
        $mail->Password   = 'Doux13579.';                               //SMTP password
        $mail->SMTPSecure = 'tls';         //Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
        $mail->Port       = 587;                                    //TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

        //Recipients
        $mail->setFrom('douxtb@gmail.com', 'http://douxtb.cl/');//(quien envia)
        $mail->addAddress($correo);     //Add a recipient(a quien se le va a enviar)


        //Content
        $mail->isHTML(true);                                  //Set email format to HTML
        $mail->Subject = 'Cambio de contraseña en http://douxtb.cl/';
        $saltoDeLinea="\r\n";
        $mensaje="Su nueva contraseña es: " .$pass." ".$saltoDeLinea."Ahora puede intentar iniciar secion.";

        $mail->Body    = $mensaje;


        $mail->send();
        
        echo '<script>alert ("Se envio una nueva contraseña a '.$correo.'");
        location.href = "login.php";
        </script>';

        //(para que el \n funcione nescesita "" en lugar de '')
    } catch (Exception $e) {
        echo "Hubo un error al enviar el correo con la nueva contraseña. Mailer Error: {$mail->ErrorInfo}";
    }

 }

?>