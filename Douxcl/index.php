<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="author" content="Leonardo Exequiel Canga Salinas" />
	<meta name="copyright" content="Leonardo Exequiel Canga Salinas" />
        
	<title>Doux</title>

	<link rel="stylesheet" href="bootstrap-5.0.0-beta2-dist/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css\estilos.css">
  <link rel="stylesheet" type="text/css" href="css/EstilosMax750.css">   
  <link rel="stylesheet" href="css\whatsapp.css">
  <!-- <link  rel="icon"   href="" type="" /> -->
  <link rel="shortcut icon" type="image/x-icon" href="img\Iconos\doux.ico"/>

</head>
	<body>

<!-- --------------------------------boton whatsapp--------------------------- -->
         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <a href="https://api.whatsapp.com/send?phone=56936258826&text=Hola%21%20Quisiera%20m%C3%A1s%20informaci%C3%B3n%20sobre%20el%20producto..." class="float" target="_blank">
        <i class="fa fa-whatsapp my-float"></i>
        </a>

<!-- --------------------------------header----------------------------------- -->
        <div id="elemHeader"></div>
        <script src="js\menu.js"></script>

<!-- --------------------------------------carrusel---------------------------- -->
        <div id="elemCarrusel"></div>
        <script src="js/carrusel.js">                 
        </script>
<!-- ------------------------------------galeria--------------------------------- -->
     <div class="container gallery-container" id="galeria">

        <h1>Destacados</h1>
        
        <div class="tz-gallery">

            <div class="row">
                <div class="col-sm-6 col-md-4">
                    <a class="lightbox" href="paginas/CuidadoCorporal/CuidadoCorpProd11.html">
                        <img src="img/cuidadoCorporal/21.jpg" alt="Benches">
                        <h4>Mascarilla negra </h4>
                        <!-- <h5>$3.990 por unidad</h5> -->
                        <h5>$2.000 por mayor</h5>
                    </a>
                </div>
                <div class="col-sm-6 col-md-4">
                    <a class="lightbox" href="paginas/Maquillaje/MaquillajeProd12.html">
                        <img src="img/maquillaje/28.jpg" alt="Sky">
                        <h4>Delineador de ojos Stars</h4>
                        <!-- <h5>$1.990 por unidad</h5> -->
                        <h5>$750 por mayor</h5>
                    </a>
                </div>
                <div class="col-sm-6 col-md-4">
                    <a class="lightbox" href="paginas/unas/UnasProd2.html">
                        <img src="img/unas/4.jpg" alt="Rails">
                        <h4>Liquido de acrílico uso profesional</h4>
                        <!-- <h5>$3.490 por unidad</h5> -->
                        <h5>$1.500 por mayor</h5>
                    </a>
                </div>
                <div class="col-sm-6 col-md-4">
                    <a class="lightbox" href="paginas/Maquillaje/MaquillajeProd5.html">
                        <img src="img/maquillaje/15.jpg" alt="Benches">
                        <h4>Mascara de pestañas 4D</h4>
                        <!-- <h5>$2.990 por unidad</h5> -->
                        <h5>$750 por mayor</h5>
                    </a>
                </div>
                <div class="col-sm-6 col-md-4">
                    <a class="lightbox" href="paginas/unas/UnasProd6.html">
                        <img src="img/unas/8.jpg" alt="Benches">
                        <h4>Pétalos quita esmalte</h4>
                        <h5>$200 S:por mayor</h5>
                        <h5>$300 L:por mayor</h5>
                    </a>
                </div>
                <div class="col-sm-6 col-md-4">
                    <a class="lightbox" href="paginas/Accesorios/AccesoriosProd2.html">
                        <img src="img/accesorios/2.jpg" alt="Rails">
                        <h4>Encrespador</h4>
                        <!-- <h5>$2.400 precio único</h5> -->
                        <h5>$2.400 Por mayor</h5>

                        <br>
                    </a>
                </div>

        </div>

        <nav aria-label="Page navigation example" id="pagination" class="nav justify-content-center">
            <ul class="pagination ">
              <li class="page-item"><a class="page-link" href="index.php">Previous</a></li>
              <li class="page-item"><a class="page-link" href="index.php"><b>1</b></a></li>
<!--               <li class="page-item"><a class="page-link" href="paginas/pag2.html">2</a></li>
              <li class="page-item"><a class="page-link" href="paginas/pag3.html">3</a></li>
              <li class="page-item"><a class="page-link" href="paginas/pag4.html">4</a></li>
              <li class="page-item"><a class="page-link" href="paginas/pag5.html">5</a></li>
              <li class="page-item"><a class="page-link" href="paginas/pag6.html">6</a></li> -->
              <li class="page-item"><a class="page-link" href="index.php">Next</a></li>
            </ul>
        </nav>

    </div>
</div>    

<!-- --------------------------------------carrusel-2--------------------------->
      <div id="elemLoNuevo"></div>
      <script src="js/LoNuevo.js"></script>

<!-- ------------------------------------------------footer-------------------------------- -->

        <div id="ElemFooter"></div>
        <script src="js/footer.js"></script>
        
</body>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="js/codigo.js"></script>
<!-- 
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script> -->

  <script type="text/javascript" src="bootstrap-5.0.0-beta2-dist/js/bootstrap.bundle.min.js"></script>
  
</html>